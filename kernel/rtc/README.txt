Test for Real Time Clocks

This test checks if all the RTC in the system are working correctly by
setting the hwclock, checking if the since_epoch value is incremented
and firing a wake alarm IRQ.
