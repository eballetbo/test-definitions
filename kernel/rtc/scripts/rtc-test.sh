#!/bin/bash
#
# rtc-test - Test for Real Time Clocks
#

TEST_NAME="rtc-test"
RTCPATH="/sys/class/rtc"
DEVS=($(ls ${RTCPATH}))
ret=0

for rtc in ${DEVS[@]}; do
    name=$(cat ${RTCPATH}/${rtc}/name)
    echo "Testing RTC ${name}"

    echo "Set ${name} hardware clock"
    hwclock -w -f /dev/${rtc}
    ret=$?
    if [ ${ret} -ne 0 ]; then
	echo "set-hw-clock[${name}]:" "fail"
    else
	echo "set-hw-clock[${name}]:" "pass"
    fi

    echo "Check if ${name} since_epoch is incremented"
    before=$(cat ${RTCPATH}/${rtc}/since_epoch)

    sleep 1
    after=$(cat ${RTCPATH}/${rtc}/since_epoch)
    if [[ "${after}" -ne "${before} + 1" ]]; then
	ret=1
	echo "since-epoch-increment[${name}]:" "fail"
    else
	echo "since-epoch-increment[${name}]:" "pass"
    fi

    echo "Fire ${name} wakealarm IRQ"
    echo +1 > ${RTCPATH}/${rtc}/wakealarm

    sleep 1
    value=$(cat ${RTCPATH}/${rtc}/wakealarm)
    if [[ "${value}" -ne 0 ]]; then
        ret=1
        echo "fire-wake-alarm[${name}]:" "fail"
    else
	echo "fire-wake-alarm[${name}]:" "pass"
    fi
done

exit ${ret}
