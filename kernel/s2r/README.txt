Test for Suspend-to-RAM

This test checks if the system is able to enter into a suspend state
and can be resumed by using an RTC alarm IRQ as its wake up source.
