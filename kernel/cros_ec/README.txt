Test for ChromeOS Embedded Controllers

This test checks if the ChromeOS EC in the system is working correctly by
sending IOCTL commands to the EC chardev driver and reading a file from
the sysfs interface.
