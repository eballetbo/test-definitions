#!/usr/bin/python2.7
#
# cros-ec-test - Basic test for the ChromeOS Embedded Controller
#

from ctypes import *
import errno
import fcntl
import sys
import os

EC_DEV_PATH   = "/dev/cros_ec"
EC_SYSFS_PATH = "/sys/class/chromeos/cros_ec/"
EC_DEV_IOC = 0xec
EC_DEV_IOCXCMD  = 0xc014ec00  # _IOWR(EC_DEV_IOC, 0, struct cros_ec_command)
EC_DEV_IOCRDMEM = 0xc108ec01  # _IOWR(EC_DEV_IOC, 1, struct cros_ec_readmem)
EC_CMD_MAX = 0xfc
EC_MEMMAP_SIZE = 0xff
EC_CMD_HELLO = 0x01
EC_RES_SUCCESS = 0
EC_IN_DATA  = 0xa0b0c0d0 # magic number that the EC expects on HELLO
EC_OUT_DATA = 0xa1b2c3d4 # magic number that the EC answers on HELLO

exit_status = os.EX_OK
fd = None

class cros_ec_command(Structure):
    _fields_ = [
        ('version', c_uint),
        ('command', c_uint),
        ('outsize', c_uint),
        ('insize', c_uint),
        ('result', c_uint),
        ('data', c_ubyte * EC_CMD_MAX)
    ]

class cros_ec_readmem(Structure):
    _fields_ = [
        ('offset', c_uint),
        ('length', c_uint),
        ('buffer', c_ubyte * EC_MEMMAP_SIZE)
    ]

class ec_params_hello(Structure):
    _fields_ = [
        ('in_data', c_uint)
    ]

class ec_params_response(Structure):
    _fields_ = [
        ('out_data', c_uint)
    ]

print "Testing opening {0}".format(EC_DEV_PATH)

try:
    fd = open(EC_DEV_PATH, 'r')
    print "open-ec-dev: pass"
except IOError as e:
    print "I/O error({0}): {1}".format(e.errno, e.strerror)
    print "open-ec-dev: fail"
    exit_status = os.EX_IOERR

print "Testing EC_DEV_IOCXCMD cmd"

if exit_status == os.EX_OK:
    param = ec_params_hello()
    param.in_data = EC_IN_DATA

    response = ec_params_response()

    cmd = cros_ec_command()
    cmd.version = 0x0
    cmd.command = EC_CMD_HELLO
    cmd.insize = sizeof(param)
    cmd.outsize = sizeof(response)

    memmove(addressof(cmd.data), addressof(param), cmd.outsize)

    try:
        fcntl.ioctl(fd, EC_DEV_IOCXCMD, cmd)

        memmove(addressof(response), addressof(cmd.data), cmd.insize)

        if cmd.result == EC_RES_SUCCESS and response.out_data == EC_OUT_DATA:
            print "ec-xcmd-ioctl: pass"
        else:
            print "ec-xcmd-ioctl: fail"
            exit_status = os.EX_IOERR
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        print "ec-xcmd-ioctl: fail"
        exit_status = os.EX_IOERR
else:
    print "ec-xcmd-ioctl: unknown"

print "Testing EC_DEV_IOCRDMEM cmd"

if exit_status == os.EX_OK:
    mem = cros_ec_readmem()
    mem.offset = 0x0
    mem.length = EC_MEMMAP_SIZE

    try:
        fcntl.ioctl(fd, EC_DEV_IOCRDMEM, mem)
        "ec-rdmem-ioctl: pass"
    except IOError as e:
        # Not all platforms supports direct reads, so -ENOTTY isn't an error
        if e.errno == errno.ENOTTY:
            print "ec-rdmem-ioctl: skip"
        else:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)
            print "ec-rdmem-ioctl: fail"
            exit_status = os.EX_IOERR
else:
    print "ec-rdmem-ioctl: unknown"

if fd != None:
    fd.close()

print "Testing EC sysfs interface"

try:
    fd = open(EC_SYSFS_PATH + "version", 'r')
    print "open-ec-sysfs: pass"
    fd.close()
except IOError as e:
    print "I/O error({0}): {1}".format(e.errno, e.strerror)
    print "open-ec-sysfs: fail"
    exit_status = os.EX_IOERR

sys.exit(exit_status)
